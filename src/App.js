import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';

import awsconfig from './aws-exports';

import * as queries from './graphql/queries';
import * as mutations from './graphql/mutations';
import * as subscriptions from './graphql/subscriptions';
import Auth from "@aws-amplify/auth";
import { DataStore } from '@aws-amplify/datastore';
import { Todo } from './models';

import Amplify, {API, graphqlOperation} from 'aws-amplify';
//import Auth from 'aws-amplify';
import {Connect} from "aws-amplify-react";
import { withAuthenticator } from 'aws-amplify-react';

Amplify.configure(awsconfig);


console.log("Subscription Todos");
const subscription= API.graphql(
   graphqlOperation(subscriptions.onCreateTodo)
).subscribe({
   next: (mydata) => console.log("Todo printing"+JSON.stringify(mydata))
});

const todoDetails = {
    title: 'Source not defined 22',
    content: 'Learn Amplify GraphQL Cognito',
    price: 24,
    rating: 4.5,
    year: 2017
};

//console.log("Adding a Todo");
//const newTodo =  API.graphql(graphqlOperation(mutations.createTodo, {input: todoDetails}));
//console.log("AddingTodoFinished");
//console.log(newTodo);
//console.log("==============");

//console.log("Lisitng Todos");
//const allTodos =  API.graphql(graphqlOperation(queries.listTodos));
//console.log(allTodos);
//console.log("==============");





// API.graphql(
//     graphqlOperation(subscriptions.onUpdateTodo)
// ).subscribe({
//     next: (updatetodo) => console.log("Todo printing"+updatetodo)
// });



//subscription.unsubscribe();


class AddTodo extends Component {
  constructor(props) {
    super(props);
    this.submit = this.submit.bind(this);
    this.state = {
        title: '',
        content: '',
        price: '',
        rating: ''
    };
  }

  handleChange(title, event) {
      this.setState({ [title]: event.target.value });
  }

  async submit() {
    console.log("printing props");
    console.log(this.props);
    const { onCreate } = this.props;
    const input = {
      title: this.state.title,
      content: this.state.content,
      price: this.state.price,
      rating: this.state.rating
    }
    console.log(input);

    try {
      await onCreate({input})
    } catch (err) {
      console.error(err);
    }

  }

  render(){
    return (
        <div>
            <input
                name="title"
                placeholder="title"
                onChange={(event) => { this.handleChange('title', event)}}
            />
            <input
                name="content"
                placeholder="content"
                onChange={(event) => { this.handleChange('content', event)}}
            />
            <input
                name="price"
                placeholder="price"
                onChange={(event) => { this.handleChange('price', event)}}
            />
            <input
                name="rating"
                placeholder="rating"
                onChange={(event) => { this.handleChange('rating', event)}}
            />
            <button onClick={this.submit}>
                Add
            </button>
            
        </div>
    );
  }
}

class App extends Component {

    render() {

        const ListView = ({ todos }) => (
            <div>
                <h3>All Todos</h3>
                <ul>
                    Title ------------------ Content ----------------Price --Rating
                </ul>
                <ul>
                    {todos.map(todo => <li key={todo.id}>{todo.title} ------- {todo.content} ------------ {todo.price} --{todo.rating}</li>)}
                </ul>
            </div>
        );

        return (
          <div className="App">
            <div className="App">
                <button onClick={() => Auth.federatedSignIn({provider: 'Google'})}>Open Facebook</button>
              
               
            </div>
            
            <Connect query={graphqlOperation(queries.listTodos)}
              subscription={graphqlOperation(subscriptions.onCreateTodo)}
              onSubscriptionMsg={(prev, {onCreateTodo}) => {
                  console.log('Subscription data:', onCreateTodo)
                  return prev;
                }
              }>
            {({ data: { listTodos }, loading, error }) => {
              if (error) return <h3>Error</h3>;
              if (loading || !listTodos) return <h3>Loading...</h3>;
                return (<ListView todos={listTodos.items} />);
            }}
            </Connect>

            <Connect mutation={graphqlOperation(mutations.createTodo)>
              ( mutation => (
                <AddTodo onCreate={mutation} />
              )
            </Connect>
          </div>
        )
    }
} 



export default App;
